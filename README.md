# xivo-libss7-packaging

Debian packaging for [libss7](http://www.asterisk.org/downloads/libss7) used in XiVO.

## Upgrading

To upgrade libss7:

* Update the version number in the `VERSION` file
* Update the changelog using `dch -i` to the matching version
* Push the changes
